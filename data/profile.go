package data

type GymProfile struct {
	Id            int64            `bson:"_id"`
	ChatId        int64            `bson:"chat_id"`
	Name          string           `bson:"name"`
	FitnessPoints int32            `bson:"fitness_points"`
	Exercises     map[string]int32 `bson:"exercises"`
}
