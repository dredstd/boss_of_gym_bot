package handlers

import (
	"context"
	"errors"
	"fitness_bot/data"
	"fitness_bot/service"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"strconv"
)

type HandlerContext struct {
	Profile           *data.GymProfile
	ProfileService    *service.ProfileService
	Bot               *tgbotapi.BotAPI
	Ctx               context.Context
	OverrideMessageID *int
	Parameters        *map[string]string
}

func (h *HandlerContext) TryGetStringParameter(parameterName string) (string, error) {
	if h.Parameters == nil {
		return "", errors.New("empty parameters")
	}
	result, exists := (*h.Parameters)[parameterName]
	if exists == false {
		return "", errors.New(fmt.Sprintf("not found %s", parameterName))
	}
	return result, nil
}

func (h *HandlerContext) TryGetIntParameter(parameterName string) (int32, error) {
	strValue, err := h.TryGetStringParameter(parameterName)
	if err != nil {
		return 0, err
	}
	intValue, err := strconv.Atoi(strValue)
	if err != nil {
		return 0, err
	}
	return int32(intValue), nil
}
