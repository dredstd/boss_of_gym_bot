package handlers

import (
	"fitness_bot/data"
	"strings"
)

type rating struct {
	Boss *data.GymProfile
	All  []*data.GymProfile
}

func PrintRating(handlerContext HandlerContext) error {

	all, err := handlerContext.ProfileService.GetTop(handlerContext.Ctx, 10)
	if err != nil {
		return err
	}
	rating := rating{
		Boss: all[0],
		All:  all[1:],
	}

	sb := new(strings.Builder)
	err = GetMessageTemplates().All["rating"].Execute(sb, rating)
	if err != nil {
		return err
	}
	return Print(handlerContext.Profile, handlerContext.Bot, handlerContext.OverrideMessageID, sb.String(), &RatingKeyboard)
}
