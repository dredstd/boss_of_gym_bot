package handlers

import (
	"strings"
)

func PrintProfileInfo(handlerContext HandlerContext) error {
	sb := new(strings.Builder)
	err := GetMessageTemplates().All["profile_info"].Execute(sb, handlerContext.Profile)
	if err != nil {
		return err
	}
	return Print(handlerContext.Profile, handlerContext.Bot, handlerContext.OverrideMessageID, sb.String(), &MainMenuKeyboard)
}
