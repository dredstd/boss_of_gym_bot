package handlers

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var TrainingKeyboards = map[string]tgbotapi.InlineKeyboardMarkup{
	"pull-up": GetTrainingKeyboard("pull-up"),
	"push-up": GetTrainingKeyboard("push-up"),
	"walking": GetTrainingKeyboard("walking"),
}

func GetTrainingKeyboard(exercise string) tgbotapi.InlineKeyboardMarkup {
	return tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("add 1", fmt.Sprintf("increment_{%s}_{%d}", exercise, 1)),
			tgbotapi.NewInlineKeyboardButtonData("add 3", fmt.Sprintf("increment_{%s}_{%d}", exercise, 3)),
			tgbotapi.NewInlineKeyboardButtonData("add 5", fmt.Sprintf("increment_{%s}_{%d}", exercise, 5)),
			tgbotapi.NewInlineKeyboardButtonData("add 10", fmt.Sprintf("increment_{%s}_{%d}", exercise, 10)),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("back", "main"),
		),
	)
}

var MainMenuKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Подтягивания", "training_{pull-up}"),
		tgbotapi.NewInlineKeyboardButtonData("Анжумания", "training_{push-up}"),
		tgbotapi.NewInlineKeyboardButtonData("Ходьба", "training_{walking}"),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("rating", "rating"),
	),
)

var RatingKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("back", "main"),
	),
)
