package handlers

import (
	"strings"
)

func PrintTraining(handlerContext HandlerContext) error {
	sb := new(strings.Builder)
	exercise, err := handlerContext.TryGetStringParameter("exercise")
	if err != nil {
		return err
	}

	err = GetMessageTemplates().All["training"].Execute(sb, exercise)
	if err != nil {
		return err
	}

	keyboard := TrainingKeyboards[exercise]
	return Print(handlerContext.Profile, handlerContext.Bot, handlerContext.OverrideMessageID, sb.String(), &keyboard)
}
