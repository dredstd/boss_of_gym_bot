package handlers

import (
	"fitness_bot/data"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"regexp"
	"strings"
)

var findParameters = regexp.MustCompile(`{([a-zA-Z-\d]+)}`)

type BaseHandler struct {
	MappingKey     string
	HandlerFunc    func(HandlerContext) error
	ParameterNames []string
}

func getArrayWithoutBrackets(raw []string) []string {
	for i := 0; i < len(raw); i++ {
		raw[i] = raw[i][1 : len(raw[i])-1]
	}
	return raw
}

func CreateHandler(mappingKey string, handlerFunc func(HandlerContext) error) BaseHandler {

	return BaseHandler{
		MappingKey:     mappingKey,
		HandlerFunc:    handlerFunc,
		ParameterNames: getArrayWithoutBrackets(findParameters.FindAllString(mappingKey, -1)),
	}
}

func (h *BaseHandler) IsApplicable(message string) (bool, *map[string]string) {
	if len(h.ParameterNames) == 0 {
		return h.MappingKey == message, nil
	}
	indexInMapping := strings.Index(h.MappingKey, "{")
	indexInMessage := strings.Index(message, "{")

	if indexInMapping != indexInMessage {
		return false, nil
	}

	a := message[:indexInMessage]
	b := h.MappingKey[:indexInMapping]
	if a != b {
		return false, nil
	}

	values := getArrayWithoutBrackets(findParameters.FindAllString(message, -1))
	if len(values) != len(h.ParameterNames) {

		return false, nil
	}

	parameters := map[string]string{}
	for i := 0; i < len(values); i++ {
		parameters[h.ParameterNames[i]] = values[i]
	}
	return true, &parameters
}

func Print(profile *data.GymProfile, bot *tgbotapi.BotAPI, overrideMessageID *int, message string, keyboard *tgbotapi.InlineKeyboardMarkup) error {

	if overrideMessageID != nil {
		editResponse := tgbotapi.NewEditMessageText(profile.ChatId, *overrideMessageID, message)
		editResponse.ReplyMarkup = keyboard
		_, err := bot.Send(editResponse)
		return err
	}

	response := tgbotapi.NewMessage(profile.ChatId, message)
	response.ReplyMarkup = keyboard
	_, err := bot.Send(response)
	return err
}
