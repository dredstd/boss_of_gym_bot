package handlers

import (
	"embed"
	"io/fs"
	"sync"
	"text/template"
)

type MessageTemplates struct {
	All map[string]*template.Template
}

var (
	//go:embed templates
	files embed.FS
)

var instance *MessageTemplates = nil

func exerciseTranslate(exerciseId string) string {
	switch exerciseId {
	case "pull-up":
		return "Подтягивания"
	case "push-up":
		return "Анжумания"
	case "walking":
		return "Ходьба, км"
	}
	return "Неизвостность"

}

func GetMessageTemplates() *MessageTemplates {
	var once sync.Once

	var funcMap = template.FuncMap{
		"exerciseTranslate": exerciseTranslate,
	}

	once.Do(func() {
		if instance == nil {
			tmplFiles, err := fs.ReadDir(files, "templates")
			if err != nil {
				panic(err)
			}

			messageTemplates := MessageTemplates{}
			messageTemplates.All = map[string]*template.Template{}

			for _, tmpl := range tmplFiles {
				if tmpl.IsDir() {
					continue
				}

				pt, err := template.New(tmpl.Name()).Funcs(funcMap).ParseFS(files, "templates/"+tmpl.Name())
				if err != nil {
					panic(err)
				}

				messageTemplates.All[tmpl.Name()[:len(tmpl.Name())-5]] = pt
			}
			instance = &messageTemplates
		}
	})
	return instance
}
