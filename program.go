package main

import (
	"context"
	"fitness_bot/data"
	"fitness_bot/handlers"
	"fitness_bot/service"
	"log"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var messageHandlers []handlers.BaseHandler

func main() {
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_API_TOKEN"))
	if err != nil {
		panic(err)
	}
	profileService, err := service.CreateProfileService(context.TODO(), os.Getenv("MONGO_CONNECTION"), os.Getenv("MONGO_DB_NAME"), os.Getenv("MONGO_USER"), os.Getenv("MONGO_PASSWORD"))
	if err != nil {
		panic(err)
	}
	bot.Debug = false

	// Create a new UpdateConfig struct with an offset of 0. Offsets are used
	// to make sure Telegram knows we've handled previous values, and we don't need them repeated.
	updateConfig := tgbotapi.NewUpdate(0)
	messageHandlers = []handlers.BaseHandler{
		handlers.CreateHandler("rating", handlers.PrintRating),
		handlers.CreateHandler("training_{exercise}", handlers.PrintTraining),
		handlers.CreateHandler(
			"increment_{exercise}_{count}",
			func(c handlers.HandlerContext) error {
				exerciseName, err := c.TryGetStringParameter("exercise")
				if err != nil {
					return err
				}
				count, err := c.TryGetIntParameter("count")
				if err != nil {
					return err
				}

				currentCount, _ := c.Profile.Exercises[exerciseName]
				currentCount += count
				c.Profile.Exercises[exerciseName] = currentCount

				c.Profile.FitnessPoints++
				cmdErr := c.ProfileService.SaveProfile(c.Ctx, c.Profile)
				if cmdErr != nil {
					return cmdErr
				}

				return handlers.PrintProfileInfo(c)
			},
		),
		handlers.CreateHandler(
			"decrement_{exercise}_{count}",
			func(c handlers.HandlerContext) error {
				c.Profile.FitnessPoints--
				cmdErr := c.ProfileService.SaveProfile(c.Ctx, c.Profile)
				if cmdErr != nil {
					return cmdErr
				}

				return handlers.PrintProfileInfo(c)
			},
		),
	}

	// Tell Telegram we should wait up to 30 seconds on each request for an
	// update. This way we can get information just as quickly as making many
	// frequent requests without having to send nearly as many.
	updateConfig.Timeout = 30
	updates := bot.GetUpdatesChan(updateConfig)

	for update := range updates {
		ctx := context.TODO()

		if update.Message != nil {
			profile, err := getProfile(ctx, profileService, update.Message.From, update.Message.Chat)
			if err != nil {

				log.Printf("Unable get profile %s", err)
				continue
			}

			handleMessage(ctx, profile, update.Message.Text, nil, bot, profileService)
		} else if update.CallbackQuery != nil {
			// Respond to the callback query, telling Telegram to show the user a message with the data received.
			callback := tgbotapi.NewCallback(update.CallbackQuery.ID, update.CallbackQuery.Data)
			if _, err := bot.Request(callback); err != nil {
				log.Printf("Unable to answer callback %s", err)
				continue
			}

			profile, err := getProfile(ctx, profileService, update.CallbackQuery.From, update.CallbackQuery.Message.Chat)
			if err != nil {
				log.Printf("Unable get profile %s", err)
				continue
			}
			handleMessage(ctx, profile, update.CallbackQuery.Data, &update.CallbackQuery.Message.MessageID, bot, profileService)
		}
	}
}

func getProfile(ctx context.Context, profileService *service.ProfileService, user *tgbotapi.User, chat *tgbotapi.Chat) (*data.GymProfile, error) {
	profile, err := profileService.GetProfile(ctx, user.ID)

	if err != nil {
		profile = &data.GymProfile{
			Id:            user.ID,
			ChatId:        chat.ID,
			Name:          user.FirstName,
			FitnessPoints: 0,
			Exercises:     map[string]int32{},
		}
		err := profileService.SaveProfile(ctx, profile)
		if err != nil {
			log.Printf("Unable to save profile %s", err)
			return nil, err
		}
	}
	return profile, nil
}

func handleMessage(ctx context.Context, profile *data.GymProfile, message string, overrideMessageID *int, bot *tgbotapi.BotAPI, profileService *service.ProfileService) {

	handlerContext := handlers.HandlerContext{
		Profile:           profile,
		ProfileService:    profileService,
		Bot:               bot,
		Ctx:               ctx,
		OverrideMessageID: overrideMessageID,
	}

	var cmdErr error
	for i := 0; i < len(messageHandlers); i++ {
		handler := messageHandlers[i]
		applicable, parameters := handler.IsApplicable(message)
		if applicable {
			handlerContext.Parameters = parameters
			cmdErr = handler.HandlerFunc(handlerContext)
			if cmdErr != nil {
				log.Printf("Unable handle %s : %s", message, cmdErr)
			}
			return
		}
	}

	cmdErr = handlers.PrintProfileInfo(handlerContext)
	if cmdErr != nil {
		log.Printf("Unable handle %s : %s", message, cmdErr)
		return
	}
	//msg.ReplyToMessageID = message.MessageID
}
