package service

import (
	"context"
	"fitness_bot/data"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/url"
)

type ProfileService struct {
	mongoConnectionString *string
	Collection            *mongo.Collection
	Cache                 map[int64]*data.GymProfile
}

func CreateProfileService(ctx context.Context, connectionString string, dbName string, username string, password string) (*ProfileService, error) {

	log.Printf("Connecting mongo '" + dbName + "' ...")
	profileService := new(ProfileService)

	serverAPI := options.ServerAPI(options.ServerAPIVersion1)
	credential := options.Credential{
		AuthMechanism: "SCRAM-SHA-1",
		AuthSource:    "admin",
		Username:      url.QueryEscape(username),
		Password:      url.QueryEscape(password),
	}
	opts := options.Client().ApplyURI(connectionString).SetServerAPIOptions(serverAPI).SetAuth(credential)
	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		return nil, err
	}

	log.Printf("mongo connected")
	profileService.Collection = client.Database(dbName).Collection("profiles")
	profileService.Cache = map[int64]*data.GymProfile{}

	profileFitnessPointsIndexModel := mongo.IndexModel{
		Keys: bson.D{{"fitness_points", -1}},
	}
	_, err = profileService.Collection.Indexes().CreateOne(context.TODO(), profileFitnessPointsIndexModel)
	if err != nil {
		return nil, err
	}
	return profileService, nil
}

func (p *ProfileService) GetProfile(ctx context.Context, id int64) (*data.GymProfile, error) {
	storedProfile, exists := p.Cache[id]
	if exists {
		return storedProfile, nil
	}

	findResult := p.Collection.FindOne(ctx, bson.D{{"_id", id}})
	var profile data.GymProfile
	errMarshal := findResult.Decode(&profile)
	if errMarshal != nil {
		return nil, errMarshal
	}
	if profile.Exercises == nil {
		profile.Exercises = map[string]int32{}
	}
	return &profile, nil
}

func (p *ProfileService) GetTop(ctx context.Context, count int64) ([]*data.GymProfile, error) {
	filter := bson.D{}
	opts := options.Find().SetSort(bson.D{{"fitness_points", -1}}).SetLimit(count)
	cursor, err := p.Collection.Find(ctx, filter, opts)
	var results []*data.GymProfile
	if err = cursor.All(ctx, &results); err != nil {
		return nil, err
	}
	return results, nil
}

func (p *ProfileService) SaveProfile(ctx context.Context, profile *data.GymProfile) error {
	p.Cache[profile.Id] = profile
	_, err := p.Collection.UpdateOne(ctx, bson.D{{"_id", profile.Id}}, bson.D{{"$set", profile}}, options.Update().SetUpsert(true))
	return err
}
